// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"

AMultiplayerGameMode::AMultiplayerGameMode()
{
	static ConstructorHelpers::FClassFinder<APlayerPawn> playerPawnClass(TEXT("APawn '/Game/PlayerObjects/PlayerPawnBP'"));
	if (playerPawnClass.Succeeded()) {
		pawnClass = playerPawnClass.Class;
	}
}

void AMultiplayerGameMode::SpawnPlayer(APlayerController* pc)
{
	// Get random start position
	auto start = rand() % starts.Num();

	// Set spawn parameters and spawn player pawn
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	auto pawn = GetWorld()->SpawnActor<APlayerPawn>(pawnClass, starts[start]->GetActorLocation(), starts[start]->GetActorRotation(), ActorSpawnParams);

	if (pawn) {
		// Possess pawn and activate hud
		pc->Possess(pawn);
		//InitializeHUDForPlayer(pc);
	}
}

void AMultiplayerGameMode::PostLogin(APlayerController* newController)
{
	players.Add(newController);
}

void AMultiplayerGameMode::BeginPlay()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), starts);
}
