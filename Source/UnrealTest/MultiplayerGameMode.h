// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "PlayerPawn.h"


#include "MultiplayerGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UNREALTEST_API AMultiplayerGameMode : public AGameModeBase
{
	GENERATED_BODY()


	AMultiplayerGameMode();

private:
	TArray<APlayerController*> players;
	TArray<AActor*> starts;
	TSubclassOf<APlayerPawn> pawnClass;

protected:
	virtual void PostLogin(APlayerController* newController) override;

	virtual void BeginPlay() override;

public:
	void SpawnPlayer(APlayerController* pc);
};
