// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "BurningDamageType.generated.h"

/**
 * 
 */
UCLASS()
class UNREALTEST_API UBurningDamageType : public UDamageType
{
	GENERATED_BODY()
	

public:
	int duration = 3;
	float rate = .5f;
	int damage = 5;
};
