// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MultiplayerHUD.generated.h"


class APlayerPawn;

/**
 * 
 */
UCLASS()
class UNREALTEST_API AMultiplayerHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	// Set the player pawn for the health bar
	UFUNCTION(BlueprintImplementableEvent)
	void SetPlayerPawn(APlayerPawn* pawn);

public:
};
