// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerPlayerController.h"
#include <Kismet/GameplayStatics.h>
#include "MultiplayerGameMode.h"
#include "MultiplayerHUD.h"

AMultiplayerPlayerController::AMultiplayerPlayerController()
{
}

void AMultiplayerPlayerController::SpawnPawn_Implementation()
{
	auto mode = Cast<AMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (mode) {
		// Spawn player pawn
		mode->SpawnPlayer(this);
		auto pawn = Cast<APlayerPawn>(GetPawn());
		if (pawn) {
			auto hud = Cast<AMultiplayerHUD>(GetHUD());
			if (hud) {
				hud->SetPlayerPawn(pawn);
			}
		}
	}
}

void AMultiplayerPlayerController::BeginPlay()
{
	if (IsLocalController())
	{
		ClientSetHUD(hudClass);
		SpawnPawn();
	}
	
}

void AMultiplayerPlayerController::Respawn()
{
	// Set timer to spawn new player pawn
	GetWorld()->GetTimerManager().SetTimer(respawnTimer, this, &AMultiplayerPlayerController::SpawnPawn, 2, false);
	
}
