// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MultiplayerPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class UNREALTEST_API AMultiplayerPlayerController : public APlayerController
{
	GENERATED_BODY()


public:
	AMultiplayerPlayerController();

private:
	FTimerHandle respawnTimer;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AMultiplayerHUD> hudClass;
private:
	// Spawn a player pawn
	UFUNCTION(Server, reliable)
	void SpawnPawn();
protected:
	virtual void BeginPlay();

public:
	// Request respawn of player pawn
	void Respawn();
};
