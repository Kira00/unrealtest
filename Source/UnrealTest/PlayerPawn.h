// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
//#include "GameFramework/Character.h"

#include "InputActionValue.h"
#include "Bullet.h"


#include "PlayerPawn.generated.h"

class UInputAction;

UCLASS()
class UNREALTEST_API APlayerPawn : public APawn 
{

	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

private:
	void ResetShot();
	UFUNCTION(Server, reliable)
	void TakeDamage(AActor* DamagedActor, float damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	void Die();

	UFUNCTION()
	void ApplyTickDamage();
	UFUNCTION()
	void StopTickDamage();

	UFUNCTION()
	void StopSpecialDamage();


	// Update Position/Rotation on server
	UFUNCTION(Server, reliable)
	void UpdateTransform(FVector newLocation, FRotator newRotation);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const;

	void Move(const FInputActionValue& Value);
	void Look(const FInputActionValue& Value);
	UFUNCTION(Server, reliable)
	void Fire();

	virtual void Restart() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	

private:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	int maxHealth = 100;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), replicated)
	int currentHealth;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, Category = Input)
	UInputAction* MoveAction;
	UPROPERTY(EditAnywhere, Category = Input)
	UInputAction* LookAction;
	UPROPERTY(EditAnywhere, Category = Input)
	UInputAction* FireAction;
	UPROPERTY(EditAnywhere, Category = Input)
	UInputAction* JumpAction;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* playerMesh;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* gunMesh;
	UPROPERTY(EditAnywhere)
	USceneComponent* muzzleLocation;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ABullet> bulletClass;

	EDamageType activeDamageType = EDamageType::normal;


	bool canShoot = true;

	int tickDamage;

	// Timers
	FTimerHandle shotDelayTimer;
	FTimerHandle tickDamageTimer;
	FTimerHandle tickDurationTimer;
	FTimerHandle stopSpecialDamage;


public:
	// Set the damage type of the shots
	void SetDamageType(EDamageType type);
	UFUNCTION(Client, reliable)
	void setupInput();
};
