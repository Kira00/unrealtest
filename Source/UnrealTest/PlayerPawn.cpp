// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"

#include <EnhancedPlayerInput.h>
#include <EnhancedInputComponent.h>
#include "EnhancedInputSubsystems.h"

#include "InputAction.h"
#include "BurningDamageType.h"

#include <Kismet/GameplayStatics.h>
#include "MultiplayerPlayerController.h"
#include <Net/UnrealNetwork.h>


// Sets default values
APlayerPawn::APlayerPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	playerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlayerMesh"));
	gunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	muzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));

	RootComponent = playerMesh;
	gunMesh->SetupAttachment(RootComponent);
	muzzleLocation->SetupAttachment(gunMesh);

	bReplicates = true;
}

void APlayerPawn::ResetShot()
{
	canShoot = true;
}

void APlayerPawn::TakeDamage_Implementation(AActor* DamagedActor, float damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	// Check if damage type is burning damage
	if (auto fireDamage = Cast<UBurningDamageType>(DamageType)) {
		auto world = GetWorld();
		// Set timer for tick rate
		world->GetTimerManager().SetTimer(tickDamageTimer, this, &APlayerPawn::ApplyTickDamage, fireDamage->rate, true, fireDamage->rate);
		tickDamage = damage;
		// Set timer for stopping tick damage
		world->GetTimerManager().SetTimer(tickDurationTimer, this, &APlayerPawn::StopTickDamage, fireDamage->duration, true);
	}
	// Do damage
	currentHealth -= damage;
	// Check if health is depleted
	if (currentHealth <= 0) {
		// Call death routine
		Die();
	}
}

void APlayerPawn::Die()
{
	auto controller = Cast<AMultiplayerPlayerController>(GetController());
	if (controller) {
		// Request respawn of pawn
		controller->Respawn();
	}
	// Destroy this pawn
	Destroy();
}

void APlayerPawn::ApplyTickDamage()
{
	UGameplayStatics::ApplyDamage(this, tickDamage, GetInstigatorController(), this, UDamageType::StaticClass());

}

void APlayerPawn::StopTickDamage()
{
	GetWorld()->GetTimerManager().ClearTimer(tickDamageTimer);
}

void APlayerPawn::StopSpecialDamage()
{
	activeDamageType = EDamageType::normal;
}

void APlayerPawn::UpdateTransform_Implementation(FVector newLocation, FRotator newRotation)
{
	// Send current position to server
	SetActorLocation(newLocation);
	SetActorRotation(newRotation);
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	currentHealth = maxHealth;

	OnTakeAnyDamage.AddDynamic(this, &APlayerPawn::TakeDamage);
}

void APlayerPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APlayerPawn, currentHealth);
}

void APlayerPawn::Move(const FInputActionValue& Value)
{
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr) {
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
		UpdateTransform(GetActorLocation(), GetActorRotation());
	}
}

void APlayerPawn::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr) {
		// add yaw input to controller
		AddControllerYawInput(LookAxisVector.X);
		UpdateTransform(GetActorLocation(), GetActorRotation());

	}
}

void APlayerPawn::Fire_Implementation()
{
	// Check if pawn is currently allowed to shoot
	if (canShoot) {
		auto world = GetWorld();
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		auto bullet = world->SpawnActor<ABullet>(bulletClass, muzzleLocation->GetComponentLocation(), muzzleLocation->GetComponentRotation(), ActorSpawnParams);

		if (bullet) {
			// Set damage type for the bullet
			bullet->setDamageType(activeDamageType);
			// Set delay for the next shot
			world->GetTimerManager().SetTimer(shotDelayTimer, this, &APlayerPawn::ResetShot, 0.25, false);
			canShoot = false;
		}
	}
}

void APlayerPawn::Restart()
{
	setupInput();
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Controller)	{

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		// get up vector 
		const FVector UpDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Z);
		AddMovementInput(UpDirection, -1);
		UpdateTransform(GetActorLocation(), GetActorRotation());

	}
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (UEnhancedInputComponent* enhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		enhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &APlayerPawn::Move);
		enhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &APlayerPawn::Look);
		enhancedInputComponent->BindAction(FireAction, ETriggerEvent::Triggered, this, &APlayerPawn::Fire);
	}
}

void APlayerPawn::SetDamageType(EDamageType type)
{
	activeDamageType = type;
	GetWorld()->GetTimerManager().SetTimer(stopSpecialDamage, this, &APlayerPawn::StopSpecialDamage, 5, false);
}

void APlayerPawn::setupInput_Implementation()
{
	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

