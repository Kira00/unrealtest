// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupActor.h"
#include "PlayerPawn.h"

// Sets default values
APickupActor::APickupActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("sphereMesh"));
	RootComponent = sphereMesh;

}

// Called when the game starts or when spawned
void APickupActor::BeginPlay()
{
	Super::BeginPlay();
	sphereMesh->OnComponentBeginOverlap.AddDynamic(this, &APickupActor::BeginOverlap);
	
}

// Called every frame
void APickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupActor::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Check if colliding with a player
	auto player = Cast<APlayerPawn>(OtherActor);
	if (player)
	{
		// Set timer for respawn
		GetWorld()->GetTimerManager().SetTimer(resetHandle, this, &APickupActor::ResetActor, 5, false);
		SetActorHiddenInGame(true);
		sphereMesh->OnComponentBeginOverlap.RemoveDynamic(this,&APickupActor::BeginOverlap);
		// Set damage type for player
		player->SetDamageType(damageType);
	}
}

void APickupActor::ResetActor()
{
	sphereMesh->OnComponentBeginOverlap.AddDynamic(this, &APickupActor::BeginOverlap);
	SetActorHiddenInGame(false);
}

