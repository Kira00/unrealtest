// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include <Kismet/GameplayStatics.h>
#include "BurningDamageType.h"



// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	bulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("bulletMesh"));
	RootComponent = bulletMesh;
}

void ABullet::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Call ApplyDamage based on damageType
	switch (damageType)
	{
	case EDamageType::normal:
		UGameplayStatics::ApplyDamage(OtherActor, damage, GetInstigatorController(), this, UDamageType::StaticClass());
		break;
	case EDamageType::fire:
		UGameplayStatics::ApplyDamage(OtherActor, damage, GetInstigatorController(), this, UBurningDamageType::StaticClass());
		break;
	default:
		break;
	}

	Destroy();
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	bulletMesh->OnComponentBeginOverlap.AddDynamic(this, &ABullet::BeginOverlap);
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABullet::setDamageType(EDamageType type)
{
	damageType = type;

	// set damage based on damage type
	switch (damageType)
	{
	case EDamageType::normal:
		damage = 15;
		break;
	case EDamageType::fire:
		damage = 10;
		break;
	default:
		break;
	}
}

