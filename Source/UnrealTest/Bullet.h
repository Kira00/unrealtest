// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

UENUM()
enum class EDamageType : uint8 {
	normal,
	fire
};


UCLASS()
class UNREALTEST_API ABullet : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABullet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Set the type of damage the bullet will make on target
	void setDamageType(EDamageType type);

private:

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* bulletMesh;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	int damage;

	EDamageType damageType;

private:
	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};