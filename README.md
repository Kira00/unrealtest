# UnrealTest

As requested the project provides 2 pawns being able to shoot and kill each other
The dead pawn will respawn after a short delay at one of the spawn points

Pawns are able to move in 2 dimensions and rotate along 1 axis

Unreal starter content was added to get access to some static meshes


Personal addition:
A pickup that gives a player a special bullet for a limited time
	- Player hit by bullet takes damage over time for limited duration
	- Pickup gets deactivated and activates again after delay
	
A healthbar as ui element
	- currently only visible for hosting player